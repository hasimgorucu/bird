import 'package:bird/views/sign_up.dart';
import 'package:flutter/material.dart';
import '../services/auth.dart';
import 'home.dart';

class LandingPage extends StatefulWidget {
  const LandingPage({
    super.key,
  });

  @override
  State<LandingPage> createState() => _LandingPageState();
}

class _LandingPageState extends State<LandingPage> {
  @override
  Widget build(BuildContext context) {
    return StreamBuilder(
        stream: Auth().authStateChanges,
        builder: ((context, snapshot) {
          if (snapshot.hasData) {
            return HomePage(user: Auth().currentUser);
          } else {
            return const SignUpPage();
          }
        }));
  }
}
