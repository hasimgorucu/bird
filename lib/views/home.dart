import 'package:bird/styles/colors.dart';
import 'package:bird/widgets/home/sliver_appbar.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

class HomePage extends StatefulWidget {
  final User? user;
  const HomePage({super.key, required this.user});

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
          decoration: BoxDecoration(
              gradient: LinearGradient(
                  colors: [AppColors.primary, AppColors.secondary],
                  begin: Alignment.topCenter,
                  end: Alignment.bottomCenter)),
          child: const SafeArea(child: AppSliverAppBar())),
      floatingActionButton: FloatingActionButton(
        elevation: 10,
        onPressed: (() {}),
        backgroundColor: AppColors.primary,
        child: const Icon(Icons.message),
      ),
    );
  }
}
