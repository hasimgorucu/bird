import 'package:bird/services/auth.dart';
import 'package:bird/constants/assetpaths.dart';
import 'package:bird/constants/strings.dart';
import 'package:bird/styles/colors.dart';
import 'package:bird/styles/textstyles.dart';
import 'package:bird/widgets/buttons.dart';
import 'package:bird/widgets/text_field.dart';
import 'package:flutter/material.dart';

class SignUpPage extends StatefulWidget {
  const SignUpPage({super.key});

  @override
  State<SignUpPage> createState() => _SignUpPageState();
}

class _SignUpPageState extends State<SignUpPage> {
  TextEditingController nameController = TextEditingController();
  TextEditingController emailController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  List<String> labels = ["Name", "E-mail", "Password"];
  late List<TextEditingController> controllers;
  List<bool> obscures = [false, false, true];

  @override
  void initState() {
    super.initState();
    controllers = [nameController, emailController, passwordController];
  }

  @override
  Widget build(BuildContext context) {
    double heightPixel = MediaQuery.of(context).size.height / 812;
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        decoration: const BoxDecoration(
            image: DecorationImage(
                image: AssetImage("${AssetPath.backgrounds}signup.png"),
                fit: BoxFit.cover)),
        child: Column(
          children: [
            SizedBox(
              height: heightPixel * 96,
            ),
            Image.asset(
              "${AssetPath.appImages}logo_orange.png",
              width: heightPixel * 160,
              height: heightPixel * 160,
              fit: BoxFit.contain,
            ),
            SizedBox(
              height: heightPixel * 40,
            ),
            Text(englishStrings["sign_up"],
                style: AppTextStyles.h1(heightPixel, AppColors.white)),
            SizedBox(
              height: heightPixel * 24,
            ),
            for (int i = 0; i < labels.length; i++)
              AppTextField(
                  labeltext: labels[i],
                  obscure: obscures[i],
                  controller: controllers[i]),
            SizedBox(
              height: heightPixel * 4,
            ),
            Buttons.signButtton(() {
              Auth().signInAnonymous();
            }, englishStrings["sign_up"], heightPixel),
            Buttons.textButtton(() {}, englishStrings["sign_in"], heightPixel),
            SizedBox(
              height: heightPixel * 24,
            ),
            Image.asset(
              "${AssetPath.appImages}or.png",
              fit: BoxFit.contain,
            ),
            SizedBox(
              height: heightPixel * 16,
            ),
            Buttons.googleButtton(() {})
          ],
        ),
      ),
    );
  }
}
