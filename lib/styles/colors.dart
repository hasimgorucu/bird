import 'package:flutter/material.dart';

class AppColors {
  static Color white = const Color(0XFFFFFFFF);
  static Color black = const Color(0XFF000000);
  static Color primary = const Color(0XFFC75539);
  static Color secondary = const Color(0XFFE5AEA1);
  static Color unselectedTab = const Color(0x8AFFFFFF);
}
