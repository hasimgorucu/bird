import 'package:flutter/material.dart';

class AppTextStyles {
  static TextDecoration textDecoration = TextDecoration.none;

  static TextStyle h1(double size, Color color) {
    return TextStyle(
        color: color,
        fontSize: size * 40,
        fontFamily: "Regular",
        decoration: textDecoration);
  }

  static TextStyle reg16(double size, Color color) {
    return TextStyle(
        color: color,
        fontSize: size * 16,
        fontFamily: "Regular",
        decoration: textDecoration);
  }

  static TextStyle bold16(double size, Color color) {
    return TextStyle(
        color: color,
        fontSize: size * 16,
        fontFamily: "Bold",
        decoration: textDecoration);
  }

  static TextStyle bold14(double size, Color color) {
    return TextStyle(
        color: color,
        fontSize: size * 14,
        fontFamily: "Bold",
        decoration: textDecoration);
  }

  static TextStyle reg14(double size, Color color) {
    return TextStyle(
        color: color,
        fontSize: size * 14,
        fontFamily: "Regular",
        decoration: textDecoration);
  }
}
