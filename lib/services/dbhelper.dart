import 'package:cloud_firestore/cloud_firestore.dart';

class DbHelper {
  static final FirebaseFirestore firestore = FirebaseFirestore.instance;
  static Stream<QuerySnapshot<Map<String, dynamic>>> chatSnapshots() {
    return firestore.collection("chats").snapshots();
  }
}
