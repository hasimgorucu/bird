import 'package:bird/styles/colors.dart';
import 'package:bird/styles/textstyles.dart';
import 'package:flutter/material.dart';
import 'package:bird/constants/assetpaths.dart';

class Buttons {
  //SignButton
  static GestureDetector signButtton(
      void Function() onPressed, String text, double size) {
    return GestureDetector(
        onTap: onPressed,
        child: Container(
          height: size * 40,
          width: size * 160,
          alignment: Alignment.center,
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10),
              gradient: LinearGradient(
                  colors: [AppColors.white, AppColors.secondary],
                  begin: Alignment.bottomCenter,
                  end: Alignment.topCenter)),
          child: Text(
            text,
            style: AppTextStyles.bold16(size, AppColors.primary),
          ),
        ));
  }

  //Text Buttton
  static TextButton textButtton(
      void Function() onPressed, String text, double size) {
    return TextButton(
        onPressed: onPressed,
        child: Text(
          text,
          style: AppTextStyles.bold16(size, AppColors.white),
        ));
  }

  //Google Button
  static GestureDetector googleButtton(void Function() onPressed) {
    return GestureDetector(
      onTap: onPressed,
      child: Image.asset(
        "${AssetPath.appImages}google.png",
        fit: BoxFit.contain,
      ),
    );
  }
}
