import 'package:bird/styles/colors.dart';
import 'package:bird/styles/textstyles.dart';
import 'package:flutter/material.dart';

class AppTextField extends StatelessWidget {
  const AppTextField(
      {super.key,
      required this.labeltext,
      required this.obscure,
      required this.controller});
  final String labeltext;
  final bool obscure;
  final TextEditingController controller;

  @override
  Widget build(BuildContext context) {
    double heightPixel = MediaQuery.of(context).size.height / 812;
    double widthPixel = MediaQuery.of(context).size.width / 375;
    return Container(
      margin: EdgeInsets.symmetric(vertical: heightPixel * 8),
      height: heightPixel * 48,
      width: widthPixel * 320,
      child: TextFormField(
          controller: controller,
          obscureText: obscure,
          decoration: InputDecoration(
            filled: false,
            labelStyle: AppTextStyles.reg16(heightPixel, AppColors.white),
            labelText: labeltext,
            enabledBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(10),
                borderSide: BorderSide(color: AppColors.white, width: 2)),
            focusedBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(10),
                borderSide: BorderSide(color: AppColors.white, width: 2)),
            border: OutlineInputBorder(
                borderRadius: BorderRadius.circular(10),
                borderSide: BorderSide(color: AppColors.white, width: 2)),
          )),
    );
  }
}
