import 'package:flutter/material.dart';

import '../../styles/colors.dart';
import '../../styles/textstyles.dart';
import 'chat_stream.dart';

class AppTabbar extends StatefulWidget {
  const AppTabbar({super.key});

  @override
  State<AppTabbar> createState() => _AppTabbarState();
}

class _AppTabbarState extends State<AppTabbar>
    with SingleTickerProviderStateMixin {
  late TabController tabController;
  @override
  void initState() {
    super.initState();
    tabController = TabController(length: 4, vsync: this, initialIndex: 0);
  }

  @override
  Widget build(BuildContext context) {
    double heightPixel = MediaQuery.of(context).size.height / 812;
    return Column(
      children: [
        SizedBox(
          child: TabBar(
            labelStyle: AppTextStyles.bold16(heightPixel, AppColors.white),
            labelColor: AppColors.white,
            indicatorColor: AppColors.white,
            unselectedLabelColor: AppColors.unselectedTab,
            controller: tabController,
            tabs: const [
              Tab(text: "All", icon: Icon(Icons.groups_2)),
              Tab(text: "Love", icon: Icon(Icons.favorite)),
              Tab(text: "Friends", icon: Icon(Icons.emoji_people)),
              Tab(text: "Work", icon: Icon(Icons.work)),
            ],
          ),
        ),
        Expanded(
          child: Container(
            color: AppColors.white,
            child: TabBarView(
              controller: tabController,
              children: const [
                ChatStream(),
                ChatStream(),
                ChatStream(),
                ChatStream(),
              ],
            ),
          ),
        ),
      ],
    );
  }
}
