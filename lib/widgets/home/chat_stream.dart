import 'package:bird/styles/colors.dart';
import 'package:bird/styles/textstyles.dart';
import 'package:bird/views/chat_page.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import "package:flutter/material.dart";

import '../../services/dbhelper.dart';

class ChatStream extends StatefulWidget {
  const ChatStream({super.key});

  @override
  State<ChatStream> createState() => _ChatStreamState();
}

class _ChatStreamState extends State<ChatStream> {
  @override
  Widget build(BuildContext context) {
    double heightPixel = MediaQuery.of(context).size.height / 812;
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: StreamBuilder(
        stream: DbHelper.chatSnapshots(),
        builder: (context, AsyncSnapshot<QuerySnapshot> snapshot) {
          if (snapshot.hasError) {
            return const Center(child: Text("Bir hata oluştu"));
          } else if (snapshot.connectionState == ConnectionState.waiting) {
            return Center(
              child: SizedBox(
                  width: 60,
                  height: 60,
                  child: CircularProgressIndicator(
                    color: AppColors.primary,
                  )),
            );
          } else {
            return ListView(
              children: snapshot.data!.docs
                  .map((doc) => Card(
                        borderOnForeground: true,
                        elevation: 2,
                        child: ListTile(
                          leading: CircleAvatar(
                            maxRadius: heightPixel * 25,
                            backgroundImage: const NetworkImage(
                                "https://placekitten.com/200/200"),
                          ),
                          title: Text(
                            doc["name"],
                            style: AppTextStyles.bold16(
                                heightPixel, AppColors.black),
                          ),
                          subtitle: Text(doc["messages"],
                              style: AppTextStyles.reg14(
                                  heightPixel, AppColors.black)),
                          trailing: Column(
                              mainAxisAlignment: MainAxisAlignment.spaceAround,
                              children: [
                                Text(
                                  "19:30",
                                  style: AppTextStyles.bold14(
                                      heightPixel, AppColors.primary),
                                ),
                                Container(
                                  width: heightPixel * 24,
                                  height: heightPixel * 24,
                                  decoration: BoxDecoration(
                                      color: AppColors.secondary,
                                      shape: BoxShape.circle),
                                  alignment: Alignment.center,
                                  child: Text(
                                    "3",
                                    style: AppTextStyles.bold14(
                                        heightPixel, AppColors.white),
                                  ),
                                )
                              ]),
                          onTap: () {
                            Navigator.push(context,
                                CupertinoPageRoute(builder: (_) {
                              return const ChatPage();
                            }));
                          },
                        ),
                      ))
                  .toList(),
            );
          }
        },
      ),
    );
  }
}
