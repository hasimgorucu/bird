import 'package:bird/widgets/home/tabbar.dart';
import 'package:flutter/material.dart';

import '../../styles/colors.dart';
import '../../styles/textstyles.dart';

class AppSliverAppBar extends StatefulWidget {
  const AppSliverAppBar({super.key});

  @override
  State<AppSliverAppBar> createState() => _AppSliverAppBarState();
}

class _AppSliverAppBarState extends State<AppSliverAppBar> {
  @override
  Widget build(BuildContext context) {
    double heightPixel = MediaQuery.of(context).size.height / 812;
    return NestedScrollView(
      headerSliverBuilder: (BuildContext context, bool innerBoxIsScrolled) {
        return [
          SliverAppBar(
            backgroundColor: Colors.transparent,
            floating: true,
            title: Text(
              "Bird",
              style: AppTextStyles.h1(heightPixel, AppColors.white),
            ),
            actions: [
              IconButton(onPressed: () {}, icon: const Icon(Icons.search)),
              IconButton(onPressed: () {}, icon: const Icon(Icons.more_vert)),
            ],
          ),
        ];
      },
      body: const AppTabbar(),
    );
  }
}
